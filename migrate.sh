#!/bin/sh

me=`git config --get user.name  | tr '[:upper:]' '[:lower:]' | sed 's/ /_/g'`
today=`date +%y%m%d`

versions=`ls -t migrations/versions/$today* 2>/dev/null`

if test "$versions" = ""
then
	revision=01
else
    prev_revision=$(echo $versions | awk -F "$today" '{print $2}' | awk -F "$me" '{print $1}')
    echo $prev_revision
    c=`expr $prev_revision + 1`
    revision=`printf "%02d\n" $c`
fi

echo flask db revision --rev-id $today$revision$me --autogenerate --message $1
flask db revision --rev-id $today$revision$me --autogenerate --message $1