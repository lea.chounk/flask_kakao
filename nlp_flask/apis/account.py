# -*- coding: utf-8 -*- 
from flask import  jsonify, request, session, current_app
from ..models.user import Account
from .. import core_bp
from ..base import db
from ..oauth import Oauth
from flask_cors import  cross_origin

@core_bp.route('/chk_admin', methods=["POST"])
def chk_admin():
    data = request.form
    id = data['admin_id']
    pw = data['admin_pw']

    aa = db.session.query(Account).filter(Account.id == id).first()
    if aa is None:
        current_app.logger.info('chkAdmin : none id')
        return 'not_id'
    else:

        check_pass = aa.check_password(password=pw)
        if check_pass == True:
            session['logged_in'] = True
            session['id'] = aa.id
            return 'true'
        else:
            return 'not_pw'


@core_bp.route('/log_out', methods=["GET"])
def log_out():
    session['logged_in'] = False

    return 'true'


@core_bp.route('/test', methods=["GET"])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def test():

    import settings

    return jsonify(
        kakao_oauth_url="https://kauth.kakao.com/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code" \
        % (settings.CLIENT_ID, settings.REDIRECT_URI)
    )




@core_bp.route('/get_arr', methods=["GET"])
def get_arr():


    # import pandas as pd


    # data = pd.read_csv("../many_user.csv")



    data = []

    for i in range(1,50):
        temp = {}
        temp['user_id'] = "anton_" + str(i)
        temp['mail'] =  "anton_" + str(i) + "test.kr" 
        # temp['cn'] = 'test_' + str(i)
        # temp['telephoneNumber'] = (i + i )

        data.append(temp)

        

    return jsonify(data = data, head = ["user_id", "mail"])
    
@core_bp.route('/kakao', methods=["GET"])
def kakao():
    
    from flask import redirect , make_response , session
    import jwt
    from flask_jwt_extended import set_access_cookies , create_access_token ,create_refresh_token , set_refresh_cookies

    code = str(request.args.get('code'))
    
    oauth = Oauth()
    auth_info = oauth.auth(code)
    user = oauth.userinfo("Bearer " + auth_info['access_token'])



    kakao_account = user["kakao_account"]

    profile = kakao_account["profile"]
    name = profile["nickname"]

    # session['email'] = kakao_account['email']
    # session['profile_image_url'] = profile["profile_image_url"]
    # session['name'] = name
    # session['isKakao'] = True

    # resp = make_response(render_template('index.html'))
    # access_token = create_access_token(identity=user.id)
    # refresh_token = create_refresh_token(identity=user.id)
    # resp.set_cookie("logined", "true")
    # set_access_cookies(resp, access_token)
    # set_refresh_cookies(resp, refresh_token)


    # payload = {
    #             'user_id': kakao_account['email'], # user id
    #             'name': name,
    #             'isKakao': True,
    # }
    # token = jwt.encode(payload, current_app.config['JWT_SECRET_KEY'], 'HS256')

    # response = jsonify()
    # response.set_cookie("aaaaa", token, httponly=True, secure=True)
    # set_access_cookies(response, auth_info['access_token'])
    # set_refresh_cookies(response, auth_info['access_token'])

    # response = make_response('<script>location.replace("/")</script>', 200)
    # response.set_cookie("access_token", token, httponly=True, secure=True)

    # resp = jsonify( {
    #             'user_id': kakao_account['email'], # user id
    #             'name': name,
    #             'isKakao': True,
    # } )

    
    resp = make_response(redirect('http://127.0.0.1:8081/kakao'))
    access_token = create_access_token(identity=kakao_account['email'])
    # refresh_token = create_refresh_token(identity=kakao_account['email'])

    resp.set_cookie("email", kakao_account['email'])
    resp.set_cookie("profile_image_url", profile["profile_image_url"])
    resp.set_cookie("name", name)
    resp.set_cookie("isKakao", "true")
    resp.set_cookie("access_token", access_token)
    # set_access_cookies(resp, access_token)

    # set_refresh_cookies(resp, refresh_token)




    return resp

    # return jsonify( {
    #             'user_id': kakao_account['email'], # user id
    #             'name': name,
    #             'isKakao': True,
    #             'profile_image_url':profile["profile_image_url"],
    # } )



@core_bp.route('/get_kakao', methods=["GET"])
def get_kakao():
    
    return jsonify( {
                'user_id': session['email'],
                'isKakao': session['isKakao'],
                'name': session['name'],
                'profile_image_url': session['profile_image_url'],
    } )


@core_bp.route('/get_file', methods=["POST"])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def get_file():
    
    file = request.files['photo']
    filename = file.filename
    file_full_path = '%s_%s'   %(str("test") ,filename) 
    file.save(file_full_path)

    return '0'
