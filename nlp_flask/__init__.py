import os
from flask import Blueprint
from .setup import init
from .base import app
from flask_cors import CORS
from flask_jwt_extended import JWTManager


app = init(instance_path=os.path.dirname(__file__) + '/../instance')
app.config['CORS_HEADERS'] = 'Content-Type'


core_bp = Blueprint('core', 'core_bp', template_folder=os.path.dirname(__file__) + '/templates')

CORS(app, supports_credentials=True,resources={r"/foo": {"origins": "*"}})
jwt = JWTManager(app)

with app.app_context():

    import datetime
    import traceback
    from flask import request, make_response, render_template, current_app
    from . import logging, apis, views, models, command

    app.register_blueprint(core_bp)
    app.permanent_session_lifetime = datetime.timedelta(minutes=1440)


    def app_render_template(template, **kwargs):
        """
        render template setting
        """
        response = make_response(render_template(template, **kwargs), 200)
        return response


    @app.before_request
    def before_request():
        """
        request 요청 전 호출
        """
        if request.endpoint != 'static':
            current_app.logger.info('%s -> %s  %s(API)'\
                                % ( request.endpoint,  request.json ,request.path))


    @app.errorhandler(Exception)
    def internal_error(e):
        """
        Exception 에러 공통 처리
        """
        ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        message = 'Exception (500) : \n\n%s\n%s\n주소:%s\nIP:%s\nURL:%s\n'\
                    % (traceback.format_exc(), e, request.full_path, ip, request.url)
        current_app.logger.error('500 : %s' % traceback.format_exc())
        current_app.logger.error('500 message : %s' % e)
        current_app.logger.error(message)

        return render_template('/error/500.html'), 500


    @app.errorhandler(404)
    def page_not_found(e):
        """
        404 에러 공통 처리
        """
        # current_app.logger.error('404 : %s' % request.full_path)
        return render_template('/error/404.html'), 404

    app.render_template = app_render_template
