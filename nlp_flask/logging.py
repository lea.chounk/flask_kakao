import logging
from logging import handlers
from flask import request, current_app
from . import app


corLogFormatter = logging.Formatter(
                    '%(asctime)s, [%(filename)s:%(module)s:%(funcName)s:%(lineno)d] %(message)s')


corLogHandler = handlers.RotatingFileHandler(
                    filename=app.config['FILENAME']
                    , encoding=app.config['ENCODING']
                    , maxBytes=app.config['FILE_MAX_BYTES']
                    , backupCount=app.config['BACKUPCOUNT'])


corLogHandler.setFormatter(corLogFormatter)
corLogHandler.suffix = "%Y%m%d"

current_app.logger.setLevel(logging.DEBUG)
current_app.logger.addHandler(corLogHandler)


formatter = logging.Formatter(
                '%(asctime)s, [%(filename)s:%(module)s:%(funcName)s:%(lineno)d] %(message)s')
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

current_app.logger.addHandler(handler)

db_handler = handlers.RotatingFileHandler(
                    filename=app.config['DB_FILENAME']
                    , encoding=app.config['ENCODING']
                    , maxBytes=app.config['FILE_MAX_BYTES']
                    , backupCount=app.config['BACKUPCOUNT'])

db_handler.setLevel(logging.INFO)
db_handler.setFormatter(formatter)

logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
logging.getLogger('sqlalchemy.engine').addHandler(db_handler)

current_app.logger.addHandler(db_handler)