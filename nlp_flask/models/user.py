from ..base import db
from werkzeug.security import generate_password_hash, check_password_hash



class Account(db.Model):
    __table_name__ = 'admin_account'
    id = db.Column(db.String(), primary_key=True)
    password = db.Column(db.String(), nullable=False)

    def __init__(self, id, password, **kwargs):
        self.id = id
        self.set_password(password)

    def set_password(self, password):
        self.password = generate_password_hash(password)
 
    def check_password(self, password):
        return check_password_hash(self.password, password)
