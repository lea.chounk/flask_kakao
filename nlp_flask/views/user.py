from flask import Flask, render_template, jsonify, request, session, current_app
# import pandas as pd
from ..base import app


@app.route('/', methods=["GET", "POST"])
def main():
    return render_template('login.html')