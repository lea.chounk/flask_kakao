# Default_Flask



## Getting started

## MAC 에서 PSQL  DB 만들기 
psql postgres
CREATE ROLE adapid_db WITH LOGIN PASSWORD 'dev';
ALTER ROLE adapid_db CREATEDB;
create database adapid_db;

## 가상 환경 설정
python3 -m venv venv
. sc/in
pip install --upgrade pip
pip install -r requirements.txt 


## 실행 방법
. sc/in
. sc/run 

## . sc/run 이 gunicorn 실행 백으로 돌리고 싶으면  
. sc/run &

## 기본 포트 5000
## 기본 MVC + Login 기능 끝~ 
## 받고 나서 Git Ignore 에서 settings.py 추가 하기

